import React from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';

import { Fonts } from '../utils/Fonts';

const ButtonGoAndListen = () => {
  return (
    <View style={styles.viewButton}>
      <TouchableOpacity style={styles.button}>
        <Image
          resizeMode="contain"
          style={{ width: 14, height: 14, marginRight: 15}}
          source={require('../../assets/icons/playbutton.png')}
        />
        <Text style={styles.buttonText}>Go premium and listen</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = {
  viewButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 25,
    marginRight: 25,
    marginBottom: 15,
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'rgba(105, 105, 117, 0.22)',
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 10,
    paddingBottom: 10,
  },
  buttonText: {
    fontFamily: Fonts.MontserratRegular,
    fontSize: 14,
    color: 'rgba(0, 0, 0, 1)',
  },
};

export default ButtonGoAndListen;
