import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import Slider from 'react-native-slider';

import { Fonts } from '../utils/Fonts';


const PlayerFooter = (props) => {
  return (
    <View style={styles.playerView}>
      <Slider style={styles.slider} />
      <View style={{
        width: '100%',
        paddingTop: 15,
        flexDirection: 'column',
        backgroundColor: 'rgba(255, 255, 255, 0.96)',
      }}
      >
        <View style={styles.timeView}>
          <Text style={styles.textTime}>00:00</Text>
          <Text style={styles.textTime}>00:12</Text>
        </View>
        <View style={styles.playStopView}>
          <TouchableOpacity>
            <Text style={styles.textPlayStop}>1x</Text>
          </TouchableOpacity>

          <TouchableOpacity>
            <Image
              resizeMode="contain"
              style={{ width: 20, height: 20 }}
              source={require('../../assets/icons/player/previous.png')}
            />
          </TouchableOpacity>

          <TouchableOpacity>
            <Image
              resizeMode="contain"
              style={{ width: 20, height: 20 }}
              source={require('../../assets/icons/player/playIconBlack.png')}
            />
          </TouchableOpacity>

          <TouchableOpacity>
            <Image
              resizeMode="contain"
              style={{ width: 20, height: 20 }}
              source={require('../../assets/icons/player/forward.png')}
            />
          </TouchableOpacity>

          <TouchableOpacity onPress={props.onPressChangeLettersSize}>
            <Text style={styles.textPlayStop}>A</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = {
  playerView: {
    backgroundColor: 'transparent',
    opacity: 0.95,
    width: '100%',
    position: 'absolute',
    bottom: 0,
  },
  timeView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    marginRight: 25,
    marginLeft: 25,
    marginBottom: 15,
  },
  playStopView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 15,
    marginRight: 50,
    marginLeft: 50,
  },
  textTime: {
    fontFamily: Fonts.OpenSansRegular,
  },
  textPlayStop: {
    fontFamily: Fonts.OpenSansRegular,
    fontSize: 20,
  },
  slider: {
    marginBottom: -20,
    elevation: 10,
    backgroundColor: 'transparent',
  },
};

export default PlayerFooter;
