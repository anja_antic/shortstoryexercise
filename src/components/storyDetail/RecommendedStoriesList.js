import React from 'react';
import { Text, View, Image, FlatList } from 'react-native';

import { Fonts } from '../utils/Fonts';
import RecommendedStoryCard from './RecommendedStoryCard';
// import RecommendedStoryCard from './RecommendedStoryCard';

const RecommendedStoriesList = (props) => {
  return (
    <View style={styles.viewRecommendedStories}>
      <Text style={styles.titleRecommended}>Stories that go together :</Text>
      <Text style={styles.aboutRecommended}>If you enjoyed this story, here are few more we think are an excellent
        pairing
      </Text>
      <FlatList
        horizontal
        data={props.cards}
        renderItem={({ item }) => (
          <RecommendedStoryCard card={item} />
        )}
      />
    </View>
  );
};

const styles = {
  viewRecommendedStories: {
    width: '100%',
    height: 400,
    backgroundColor: 'rgba(242, 242, 242, 1)',
    paddingTop: 10,
    paddingLeft: 25,
    paddingRight: 25,
    paddingBottom: 16,
    marginTop: 40,
    marginBottom: 15,
  },
  titleRecommended: {
    fontFamily: Fonts.PlayfairDisplayBoldItalic,
    fontSize: 24,
    marginBottom: 10,
    color: 'rgba(0, 0, 0, 1)',
  },
  aboutRecommended: {
    fontFamily: Fonts.HelveticaNeue,
    fontSize: 14,
    marginBottom: 10,
  },
};

export default RecommendedStoriesList;