import React from 'react';
import { View, TouchableOpacity, Image } from 'react-native';

const Buttons = () => {
  return (
    <View style={styles.buttonsView}>
      <TouchableOpacity>
        <Image
          style={styles.touchableButtons}
          source={require('../../assets/icons/appIcons/favouriteactivestroke.png')}
        />
      </TouchableOpacity>

      <TouchableOpacity>
        <Image
          style={[styles.touchableButtons, { marginLeft: 35, marginRight: 35 }]}
          source={require('../../assets/icons/appIcons/downloadstroke.png')}
        />
      </TouchableOpacity>

      <TouchableOpacity>
        <Image
          style={styles.touchableButtons}
          source={require('../../assets/icons/appIcons/sharestroke.png')}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = {
  buttonsView: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  touchableButtons: {
    width: 50,
    height: 50,
  },
};

export default Buttons;
