import React, { Component } from 'react';
import { Text, ScrollView, View, TouchableWithoutFeedback } from 'react-native';
import Slider from 'react-native-slider';

import { Fonts } from '../utils/Fonts';
import SectionIntroduction from './SectionIntroduction';
import PictureSection from './PictureSection';
import Buttons from './Buttons';
import RecommendedStoriesList from './RecommendedStoriesList';
import ButtonGoAndListen from './ButtonGoAndListen';
import PlayerFooter from './PlayerFooter';

const predefinedData = [
  {
    key: '1',
    name: 'SHOHAM SMITH',
    language: 'FROM: HEBREW',
    title: 'Isis - The Story Of The Goddess of Magic and Sorcery',
    picture: 'https://pbs.twimg.com/profile_images/538014983829716992/Ja7TP76P.jpeg',
  },
  {
    key: '2',
    name: 'SHOHAM SMITH',
    language: 'FROM: HEBREW',
    title: 'Isis - The Story Of The Goddess of Magic and Sorcery',
    picture: 'https://vignette.wikia.nocookie.net/lion-kinglion-guard/images/4/4a/635902362283569397796260502_' +
    'the-lion-king-poster.jpg/revision/latest?cb=20160803020414',
  },
  {
    key: '3',
    name: 'SHOHAM SMITH',
    language: 'FROM: HEBREW',
    title: 'Isis - The Story Of The Goddess of Magic and Sorcery',
    picture: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS95unYGoNm2E4qKXHJAmFJeonEJWY7oS4GbnCIUkMg8' +
    'IGK3Lauqw',
  },
  {
    key: '4',
    name: 'SHOHAM SMITH',
    language: 'FROM: HEBREW',
    title: 'Isis - The Story Of The Goddess of Magic and Sorcery',
    picture: 'https://mediartv1.freenode.ro/image/201506/w620/ariel1_24673300.jpg',
  },
  {
    key: '5',
    name: 'SHOHAM SMITH',
    language: 'FROM: HEBREW',
    title: 'Isis - The Story Of The Goddess of Magic and Sorcery',
    picture: 'https://pbs.twimg.com/profile_images/538014983829716992/Ja7TP76P.jpeg',
  },
];

class StoryFooterContainer extends Component {
  state = {
    player: true,
    storyTextSize: 14,
    introductionTitleSize: 12,
    introductionTextSize: 13,
  }

  showPlayerFooter() {
    if (this.state.player === true) {
      return (
        <PlayerFooter
          onPressChangeLettersSize={() => this.changeLettersSize()}
        />
      );
    }
    return null;
  }

  changeLettersSize() {
    if (this.state.storyTextSize === 14) {
      this.setState({ storyTextSize: 16, introductionTitleSize: 14, introductionTextSize: 15 });
    } else if (this.state.storyTextSize === 16) {
      this.setState({ storyTextSize: 18, introductionTitleSize: 16, introductionTextSize: 17 });
    } else {
      this.setState({ storyTextSize: 14, introductionTitleSize: 12, introductionTextSize: 13 });
    }
  }

  render() {
    return (
      <View>
        <ScrollView>
          <PictureSection />
          <TouchableWithoutFeedback onPress={() => this.setState({ player: !this.state.player })}>
            <View>
              <SectionIntroduction>
                <Text style={styles.authorText}>GUSTAVE FLAUBERT | FROM: FRENCH</Text>
                <Text style={styles.titleText}>MALEFICENT</Text>
                <Text style={[styles.introductionTitle, { fontSize: (this.state.introductionTitleSize) }]}>INTRODUCTION
                  BY
                  OUR EDITORS
                </Text>
                <Text style={[styles.introductionText, { fontSize: (this.state.introductionTextSize) }]}>In a world of
                  class distinctions, where madams are strolling along
                  while
                  servants are romping around; in a
                  society in which almost everyone are defined by the professional title they carry and measured in
                  terms
                  of
                  attribution and assets; there suddenly pops-up a creature of no status, no
                </Text>
                <Text style={[styles.storyText, { fontSize: (this.state.storyTextSize) }]}>For half a
                  century the housewives of Pont-l’Eveque had envied Madame Aubain her servant Felicite. For a
                  hundred francs a year, she cooked and did the housework, washed, ironed, mended, harnessed the
                  horse, fattened the poultry, made the butter and remained faithful to her mistress – although the
                  latter was by no means an agreeable person. Madame Aubain had married a comely youth without any
                  money, who died in the beginning of 1809, leaving her with two young children and a number of debts.
                  She sold all her property excepting the farm of Toucques and the farm of Geffosses, the income of
                  which barely amounted to 5,000 francs; then she left her house in Saint-Melaine, and moved into a
                  less pretentious one which had belonged to her ancestors and stood back of the market-placerr.
                </Text>
                <Text style={styles.textTags}>#affair #classics #romance #Russia #women</Text>
              </SectionIntroduction>
              <Buttons />
            </View>
          </TouchableWithoutFeedback>
          <RecommendedStoriesList
            cards={predefinedData}
          />
          <ButtonGoAndListen />
        </ScrollView>
        {this.showPlayerFooter()}
      </View>
    );
  }
}


const styles = {
  authorText: {
    fontFamily: Fonts.MontserratRegular,
    fontSize: 12,
    color: 'rgba(0, 0, 0, 1)',
  },
  titleText: {
    fontFamily: Fonts.PlayfairDisplayBold,
    fontSize: 28,
    color: 'rgba(0, 0, 0, 1)',
    marginBottom: 15,
    marginTop: 15,
  },
  introductionTitle: {
    fontFamily: Fonts.OpenSansRegular,
    fontSize: 12,
    color: '#000000',
  },
  introductionText: {
    fontFamily: Fonts.OpenSansItalic,
    fontSize: 13,
    color: 'rgba(105, 105, 117, 1)',
    marginBottom: 15,
    marginTop: 5,
  },
  storyText: {
    fontFamily: Fonts.OpenSansRegular,
    fontSize: 14,
    color: 'rgba(0, 0, 0, 0.87)',
  },
  textTags: {
    fontFamily: Fonts.PlayfairDisplayItalic,
    fontSize: 24,
    color: 'rgba(0, 0, 0, 1)',
    marginTop: 40,
    marginBottom: 40,
  },
};

export default StoryFooterContainer;
