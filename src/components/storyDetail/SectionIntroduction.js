import React from 'react';
import { View } from 'react-native';

const SectionIntroduction = ({ children }) => (
  <View style={styles.cardSectionIntroduction}>
    {children}
  </View>
);

const styles = {
  cardSectionIntroduction: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginLeft: 25,
    marginRight: 25,
    marginTop: 20,
  },
};

export default SectionIntroduction;

