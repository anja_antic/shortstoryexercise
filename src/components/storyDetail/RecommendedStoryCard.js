import React from 'react';
import { Image, Text, View } from 'react-native';

import { Fonts } from '../utils/Fonts';

const RecommendedStoryCard = (props) => {
  return (
    <View style={styles.cardOfRecommendedStory}>
      <Image
        resizeMode="cover"
        style={{ width: '100%', height: '70%' }}
        source={{ uri: props.card.picture }}
      />
      <View style={styles.viewAboutRecommendedStoryText}>
        <Text style={styles.authorTextOfRecommendedStory}>{props.card.name}</Text>
        <Text style={styles.authorTextOfRecommendedStory}>{props.card.language}</Text>
        <Text
          numberOfLines={3}
          style={styles.titleOfRecommendedStory}
        >
          {props.card.title}
        </Text>
      </View>
    </View>
  );
};

const styles = {
  cardOfRecommendedStory: {
    height: '100%',
    width: 150,
    marginRight: 20,
    backgroundColor: 'rgba(255, 255, 255, 1)',
  },
  viewAboutRecommendedStoryText: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
  },
  authorTextOfRecommendedStory: {
    fontFamily: Fonts.MontserratRegular,
    fontSize: 9,
    color: 'rgba(105, 105, 117, 1)',
  },
  titleOfRecommendedStory: {
    fontFamily: Fonts.PlayfairDisplayBold,
    fontSize: 14,
    lineHeight: 14,
    marginTop: 5,
    color: 'rgba(0, 0, 0, 1)',
  },
};

export default RecommendedStoryCard;
