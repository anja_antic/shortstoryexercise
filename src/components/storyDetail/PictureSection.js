import React from 'react';
import { View, Image, TouchableOpacity } from 'react-native';

const PictureSection = () => (
  <View style={{ height: 400 }}>
    <Image
      // resizeMode="stretch" radi i bez ovoga
      style={{ width: '100%', height: '100%' }}
      source={{
        uri: 'https://pbs.twimg.com/profile_images/507678840659775489/QdfNGNGk.jpeg',
      }}
    />
    <TouchableOpacity style={{ position: 'absolute', right: 10, top: 30 }}>
      <Image
        style={styles.touchableButtons}
        source={require('../../assets/icons/appIcons/closeonwhite.png')}
      />
    </TouchableOpacity>
    <View style={styles.buttons}>
      <TouchableOpacity>
        <Image
          style={styles.touchableButtons}
          source={require('../../assets/icons/appIcons/favouritefill.png')}
        />
      </TouchableOpacity>

      <TouchableOpacity>
        <Image
          style={[styles.touchableButtons, { marginLeft: 35, marginRight: 35 }]}
          source={require('../../assets/icons/appIcons/downloadfill.png')}
        />
      </TouchableOpacity>

      <TouchableOpacity>
        <Image
          style={styles.touchableButtons}
          source={require('../../assets/icons/appIcons/sharefill.png')}
        />
      </TouchableOpacity>
    </View>
  </View>
);

const styles = {
  touchableButtons: {
    width: 50,
    height: 50,
  },
  buttons: {
    position: 'absolute',
    flexDirection: 'row',
    justifyContent: 'center',
    bottom: 20,
    right: 30,
    left: 30,
  },
};

export default PictureSection;
